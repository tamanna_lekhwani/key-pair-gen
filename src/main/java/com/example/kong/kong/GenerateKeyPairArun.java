package com.example.kong.kong;

import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.bouncycastle.openssl.jcajce.JcaPKCS8Generator;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.Security;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.PKCS8EncodedKeySpec;

public class GenerateKeyPairArun {

    public static void main(String args[]) throws Exception{

        StringWriter pemStrWriter = new StringWriter();
        JcaPEMWriter pemWriter = new JcaPEMWriter(pemStrWriter);


        Security.addProvider(new BouncyCastleProvider());
        KeyPairGenerator g = KeyPairGenerator.getInstance("EC", "BC");
        ECGenParameterSpec spec = new ECGenParameterSpec("secp256r1");
        g.initialize(spec);
        KeyPair keyPair = g.generateKeyPair();

        pemWriter.writeObject(new JcaPKCS8Generator(keyPair.getPrivate(), null));
        pemWriter.close();
        BufferedWriter writer = new BufferedWriter(new FileWriter("privatekeyjca.pem"));
        writer.write(pemStrWriter.toString());
        writer.close();

        BufferedWriter writer2 = new BufferedWriter(new FileWriter("publickeyjca.pem"));
        StringWriter pemStrWriter2 = new StringWriter();
        JcaPEMWriter pemWriter2 = new JcaPEMWriter(pemStrWriter2);
        pemWriter2.writeObject(keyPair.getPublic());
        pemWriter2.close();
        writer2.write(pemStrWriter2.toString());
        writer2.close();


        //Below is the code user service uses to read the private key to generate the JWT

        System.out.println("---------------- :: "+pemStrWriter.toString());

        StringReader stringReader = new StringReader(pemStrWriter.toString());
        KeyFactory keyFactory = KeyFactory.getInstance("EC");
        PEMParser pemParser = new PEMParser(stringReader);
        PrivateKeyInfo kp = (PrivateKeyInfo) pemParser.readObject();
        Key key = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(kp.getEncoded()));

    }
}
