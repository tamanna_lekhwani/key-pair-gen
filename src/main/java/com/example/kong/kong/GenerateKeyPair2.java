package com.example.kong.kong;

import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.bouncycastle.openssl.jcajce.JcaPKCS8Generator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.Security;
import java.security.interfaces.ECPrivateKey;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.PKCS8EncodedKeySpec;

@RestController
public class GenerateKeyPair2 {

    @GetMapping("/create")
    public void create() throws Exception{
        StringWriter pemStrWriter = new StringWriter();
        JcaPEMWriter pemWriter = new JcaPEMWriter(pemStrWriter);


        Security.addProvider(new BouncyCastleProvider());
        KeyPairGenerator g = KeyPairGenerator.getInstance("ECDSA", "BC");
        ECGenParameterSpec spec = new ECGenParameterSpec("secp256r1");
        g.initialize(spec);
        KeyPair keyPair = g.generateKeyPair();

        pemWriter.writeObject(keyPair.getPrivate());
        pemWriter.close();
        BufferedWriter writer = new BufferedWriter(new FileWriter("privatekeyjca.pem"));
        writer.write(pemStrWriter.toString());
        writer.close();

        writer = new BufferedWriter(new FileWriter("publickeyjca.pem"));
        pemStrWriter = new StringWriter();
        pemWriter = new JcaPEMWriter(pemStrWriter);
        pemWriter.writeObject(keyPair.getPublic());
        pemWriter.close();
        writer.write(pemStrWriter.toString());
        writer.close();
    }

    @GetMapping("/hello/world")
    public String sayHello() {
        return "HEllo World";
    }

    public static void main(String args[]) throws Exception{

        StringWriter pemStrWriter = new StringWriter();
        JcaPEMWriter pemWriter = new JcaPEMWriter(pemStrWriter);


        Security.addProvider(new BouncyCastleProvider());
        KeyPairGenerator g = KeyPairGenerator.getInstance("ECDSA", "BC");
        ECGenParameterSpec spec = new ECGenParameterSpec("secp256k1");
        g.initialize(spec);
        KeyPair keyPair = g.generateKeyPair();

        pemWriter.writeObject(new JcaPKCS8Generator(keyPair.getPrivate(), null));
        pemWriter.close();
        BufferedWriter writer = new BufferedWriter(new FileWriter("privatekeyjca.pem"));
        writer.write(pemStrWriter.toString());
        writer.close();

        BufferedWriter writer2 = new BufferedWriter(new FileWriter("publickeyjca.pem"));
        StringWriter pemStrWriter2 = new StringWriter();
        JcaPEMWriter pemWriter2 = new JcaPEMWriter(pemStrWriter2);
        pemWriter2.writeObject(keyPair.getPublic());
        pemWriter2.close();
        writer2.write(pemStrWriter2.toString());
        writer2.close();

        System.out.println("---------------- :: "+pemStrWriter.toString());

        StringReader stringReader = new StringReader(pemStrWriter.toString());
        KeyFactory keyFactory = KeyFactory.getInstance("EC");
        PEMParser pemParser = new PEMParser(stringReader);
        PrivateKeyInfo kp = (PrivateKeyInfo) pemParser.readObject();
        Key key = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(kp.getEncoded()));


    }
}
