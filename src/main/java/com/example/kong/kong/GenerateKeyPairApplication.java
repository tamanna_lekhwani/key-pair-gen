package com.example.kong.kong;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GenerateKeyPairApplication {

	public static void main(String[] args) {
		SpringApplication.run(GenerateKeyPairApplication.class, args);
	}

}
