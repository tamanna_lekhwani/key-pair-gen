package com.example.kong.kong;

import com.google.common.base.Splitter;
import org.bouncycastle.jce.provider.BouncyCastleProvider;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.Security;
import java.security.spec.ECGenParameterSpec;
import java.time.ZonedDateTime;
import java.util.Base64;

public class GenerateKeyPair {

    public static void main(String args[]) throws Exception{



        Security.addProvider(new BouncyCastleProvider());
        KeyPairGenerator g = KeyPairGenerator.getInstance("ECDSA", "BC");
        ECGenParameterSpec spec = new ECGenParameterSpec("secp256r1");
        g.initialize(spec);
        KeyPair keyPair = g.generateKeyPair();

        byte[] publicKeyBytes = keyPair.getPublic().getEncoded();
        String publicKeyContent = Base64.getEncoder().encodeToString(publicKeyBytes);
        String publicKeyFormatted = "-----BEGIN PUBLIC KEY-----" + System.lineSeparator();
        for (final String row:
                Splitter
                        .fixedLength(76)
                        .split(publicKeyContent)
                )
        {
            publicKeyFormatted += row + System.lineSeparator();
        }
        publicKeyFormatted += "-----END PUBLIC KEY-----";
        //String publicKeyFormatted = "-----BEGIN PRIVATE KEY-----" + publicKeyContent + "-----END PRIVATE KEY-----";
        BufferedWriter writer = new BufferedWriter(new FileWriter("publickeytry.pem"));
        writer.write(publicKeyFormatted);
        writer.close();

        byte[] privateKeyBytes = keyPair.getPrivate().getEncoded();
        String privateKeyContent = Base64.getEncoder().encodeToString(privateKeyBytes);
        String privateKeyFormatted = "-----BEGIN PRIVATE KEY-----" + System.lineSeparator();
        for (final String row:
                Splitter
                        .fixedLength(76)
                        .split(privateKeyContent)
                )
        {
            privateKeyFormatted += row + System.lineSeparator();
        }
        privateKeyFormatted += "-----END PRIVATE KEY-----";
        //String privateKeyFormatted = "-----BEGIN PRIVATE KEY-----" + privateKeyContent + "-----END PRIVATE KEY-----";
        BufferedWriter writer2 = new BufferedWriter(new FileWriter("privatekeytry.pem"));
        writer2.write(privateKeyFormatted);
        writer2.close();



            }

}
